package fi.vamk.e1800917.SEMdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SeMdemoApplication {

	@Autowired
	private AttendanceRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SeMdemoApplication.class, args);
	}

	@Bean
	public void initDate() {
		Attendance att = new Attendance("qwerty");
		Attendance att2 = new Attendance("abc");
		Attendance att3 = new Attendance("xyz");
		repository.save(att);
		repository.save(att2);
		repository.save(att3);

	}
}
